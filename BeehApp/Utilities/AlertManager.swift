//
//  AlertManager.swift
//  BeehApp
//
//  Created by Arturo Escamilla on 10/03/21.
//

import Foundation

import UIKit

class AlertManager: NSObject {
    
    static func alert(title:String, mess:String, viewcontroller:UIViewController){
        
        let alert = UIAlertController(title: title, message: mess, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
        viewcontroller.self.present(alert, animated: true, completion: nil)
        
        
    }

}
