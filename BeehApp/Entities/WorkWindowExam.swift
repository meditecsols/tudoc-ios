//
//  WorkWindowExam.swift
//  BeehApp
//
//  Created by Arturo Escamilla on 04/04/21.
//

import UIKit

class WorkWindowExam: NSObject {

    var id : Int?
    var working_day : Bool?
    var start_time : String?
    var end_time : String?
    var exam : Int?
    var days : Array<Int>?

}

