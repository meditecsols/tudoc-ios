//
//  OrderExam.swift
//  BeehApp
//
//  Created by Arturo Escamilla on 08/04/21.
//

import UIKit

class OrderExam: NSObject {
    
    var id : Int?
    var order_id_conekta : String?
    var quantity : Int?
    var final_price : String?
    var in_site : Bool?
    var comment : String?
    var sku : String?
    var start_time : String?
    var end_time : String?
    var payment_status : String?
    var is_canceled : Bool?
    var created_on : String?
    var patient : Int?
    var exam : Int?
    var institution : Int?

}
