//
//  Exam.swift
//  BeehApp
//
//  Created by Arturo Escamilla on 11/03/21.
//

import UIKit

class Vaccine: NSObject {
    
    var id : Int?
    var name : String?
    var optional_name : String?
    var manufacturer : String?
    var prescription_needed : Bool?
    var indications : String?
    var dosage_scheme : String?
    var diseases_prevent : String?
    var contradictions : String?
    var composition : String?
    var posible_reactions : String?
    var descriptions : String?
    var vaccine_type : Int?
    var cost : Double?
    var price : Double?
    var sku:String?
    var photo : String?
    var is_active : Bool?
    var created_on : String?
    
    
    

}

