//
//  Exam.swift
//  BeehApp
//
//  Created by Arturo Escamilla on 11/03/21.
//

import UIKit

class Exam: NSObject {
    var id : Int?
    var name : String?
    var medical_name : String?
    var descriptions : String?
    var recomendations : String?
    var contraindications : String?
    var when : String?
    var how : String?
    var what_is : String?
    var when_result : String?
    var exam_type : Int?
    var cost : Double?
    var price : Double?
    var sku : String?
    var photo : String?
    var is_active : Bool?
    var created_on : String?
}

