//
//  Vaccine.swift
//  BeehApp
//
//  Created by Arturo Escamilla on 11/03/21.
//

import UIKit

class VaccineCode: NSObject {
    
    var id : Int?
    var vaccine : Vaccine?
    var comment : String?
    var created_on : String?
    var postal_code : Int?

}

