//
//  Institution.swift
//  BeehApp
//
//  Created by Arturo Escamilla on 12/04/21.
//

import UIKit

class Institution: NSObject {
    
    var id : Int?
    var name : String?
    var address : String?
    var is_active : Bool?

}
