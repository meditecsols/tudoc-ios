//
//  Workwindow.swift
//  BeehApp
//
//  Created by Arturo Escamilla on 19/03/21.
//

import UIKit

class Workwindow: NSObject {

    var id : Int?
    var working_day : Bool?
    var start_time : String?
    var end_time : String?
    var vaccine : Int?
    var days : Array<Int>?

}
