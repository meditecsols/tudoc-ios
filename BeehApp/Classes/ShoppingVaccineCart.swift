//
//  ShoppingVaccineCart.swift
//  BeehApp
//
//  Created by Arturo Escamilla on 22/03/21.
//

import Foundation
import UIKit

class ShoppingVaccineCart{
    
    var productList: [(Vaccine, Int, Double,NSArray)] //Lista: (producto,cantidad,subtotal)
    var total: Double
    
    init() {
        self.productList = []
        self.total = 0
    }
    
    func countCartItems(cart: ShoppingVaccineCart) -> Int{
        var total = 0
        for item in productList{
            total += item.1
        }
        return total
    }
    
    func calculateTotal(cart: ShoppingVaccineCart) -> Int{
        var total = 0
        for product in productList{
            total += product.1 * Int(product.0.price!) //Total = Cantidad * Precio del producto
        }
        return total
    }
    
    func printCarContents(cart: ShoppingVaccineCart){
        for product in productList{
            print("\(product.0.name ?? "")")
        }
        
    }
}
