//
//  OrderVaccineVC.swift
//  BeehApp
//
//  Created by Arturo Escamilla on 08/04/21.
//

import UIKit
import SwiftSpinner


class OrderVaccineVC: UIViewController,UITableViewDelegate,UITableViewDataSource     {
    @IBOutlet weak var tv_order: UITableView!
    @IBOutlet weak var lbl_message: UILabel!
    
    var refreshControl = UIRefreshControl()
    
    var invokeServ = InvokeService()
    let alert = AlertManager()
    
    var array_order = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tv_order.backgroundColor = UIColor.white
        self.refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tv_order.addSubview(refreshControl)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        getOrder()
    }
    
    @objc func refresh(_ sender: AnyObject) {
        getOrder()
    }
    
    func getOrder()  {
        SwiftSpinner.show("")
        invokeServ.requestOrderVaccineDataWith { (response, error, code) in
            SwiftSpinner.hide()
            if code == 200
            {
                DispatchQueue.main.async {
                self.array_order = NSMutableArray()
                if response!.count > 0
                {
                    //self.tv_vaccines.isHidden = false
                    self.lbl_message.isHidden = true
                    
                    self.array_order = self.filterData(orders: response!)
                }
                else
                {
                    //self.tv_vaccines.isHidden = true
                    self.lbl_message.isHidden = false
                
                }
                
                    self.refreshControl.endRefreshing()
                    self.tv_order.reloadData()
                }
            }
        }
        
    }
    
    func filterData(orders:NSArray) -> NSMutableArray {
        
        let array_result = NSMutableArray()
        
        for order in orders
        {
            
            let dic_order = order as! NSDictionary
            
            let order_ent = OrderVaccine()
            order_ent.id = dic_order.object(forKey: "id") as! Int?
            
            order_ent.order_id_conekta = dic_order.object(forKey: "order_id_conekta") as! String?
            order_ent.quantity = dic_order.object(forKey: "quantity") as! Int?
            
            order_ent.final_price = dic_order.object(forKey: "final_price") as! String?
            
            order_ent.in_site = dic_order.object(forKey: "in_site") as! Bool?
            
            order_ent.comment = dic_order.object(forKey: "comment") as! String?
            
    
            order_ent.start_time = dic_order.object(forKey: "start_time") as! String?
            
            order_ent.end_time = dic_order.object(forKey: "end_time") as! String?
            
            order_ent.payment_status = dic_order.object(forKey: "payment_status") as! String?
            
            order_ent.is_canceled = dic_order.object(forKey: "is_canceled") as! Bool?
            
            order_ent.created_on = dic_order.object(forKey: "created_on") as! String?
            
            order_ent.patient = dic_order.object(forKey: "patient") as! Int?
            
            order_ent.vaccine = dic_order.object(forKey: "vaccine") as! Int?
            
            order_ent.institution = dic_order.object(forKey: "institution") as! Int?
            
            array_result.add(order_ent)
        }
        
        return array_result
        
    }

    @IBAction func action_close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return array_order.count
    }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_order", for: indexPath) as! OrdersTVC
        let order_dic = array_order.object(at: indexPath.row) as! OrderVaccine
        
        invokeServ.requestVaccinebyIdDataWith(id: String(order_dic.vaccine!)) { (response, error, code) in
            if code == 200
            {
                DispatchQueue.main.async {
                    cell.lbl_name.text = (response!.object(forKey: "name") as! String)
                }
                
                
            }
        }
        
        invokeServ.requestInstitutionbyIdDataWith(id: String(order_dic.institution!)) { (response, error, code) in
            if code == 200
            {
                DispatchQueue.main.async {
                    cell.lbl_num_order.text = (response!.object(forKey: "name") as! String)
                }
                
                
            }
        }
        
        
        
        //cell.lbl_num_order.text = order_dic.order_id_conekta!
        
        
        cell.lbl_quantity.text = "Cantidad:" + String(order_dic.quantity!)
        
        cell.lbl_date.text = GlobalMethods.setDate(date: order_dic.start_time!)
        

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
