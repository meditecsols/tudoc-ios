//
//  RecoverVC.swift
//  BeehApp
//
//  Created by Arturo Escamilla on 15/04/21.
//

import UIKit
import SwiftSpinner
class RecoverVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var txtFieldEmail : UITextField!
    @IBOutlet weak var btnNext : UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))

       view.addGestureRecognizer(tap)
        
        self.txtFieldEmail.delegate = self
        self.txtFieldEmail.becomeFirstResponder()
    
        // Do any additional setup after loading the view.
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func verifyFields()
    {

        
        if self.txtFieldEmail.text!.count<=0 {
            return;
        }
        if !GlobalMethods.validateEmailWithString(email: self.txtFieldEmail.text!) {
            return;
        }
     
    }

    @IBAction func actionBack(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actionNext(_ sender: Any) {
        
        self.dismissKeyboard()
        if self.txtFieldEmail.text!.count<=0{
            AlertManager.alert(title: "TuDoc", mess: "Debes ingresar un Email", viewcontroller: self)
            return;
        }
        if !GlobalMethods.validateEmailWithString(email:self.txtFieldEmail.text!)  {
            AlertManager.alert(title: "TuDoc", mess: "Debes ingresar un Email válido", viewcontroller: self)
            return;
        }
        SwiftSpinner.show("")
        let invokeService = InvokeService()
        let dict = NSMutableDictionary()
        dict.setValue(self.txtFieldEmail.text, forKey: "email")
        
        invokeService.recoverWithData(dict) { (response, error, code) in
            if code == 201
            {
                DispatchQueue.main.async {
                    SwiftSpinner.hide()
                    
                    let alert = UIAlertController(title: "TuDoc", message: "Revise su correo electrónico, le hemos enviado las instrucciones para restaurar su contraseña.", preferredStyle: UIAlertController.Style.alert)

                    alert.addAction(UIAlertAction(title: "Entendido", style: .default, handler: { (action: UIAlertAction!) in
                        self.dismiss(animated: true, completion: nil)
                    }))



                    self.present(alert, animated: true, completion: nil)
                }
                
                
            }
            else
            {
                
                DispatchQueue.main.async {
                    SwiftSpinner.hide()
                    AlertManager.alert(title: "TuDoc", mess: "Intente de nuevo", viewcontroller: self)
                    
                }
                
            }
        }
        
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.verifyFields()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
