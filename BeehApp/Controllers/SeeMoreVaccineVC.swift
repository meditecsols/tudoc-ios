//
//  SeeMoreVaccineVC.swift
//  BeehApp
//
//  Created by Arturo Escamilla on 05/04/21.
//

import UIKit

class VaccineDetailsCell:UITableViewCell
{
    @IBOutlet weak var lbl_subject: UILabel!
    
    @IBOutlet weak var lbl_subject_text: UILabel!
}

class SeeMoreVaccineVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_optional_name: UILabel!
    @IBOutlet weak var lbl_manufacturer: UILabel!
    @IBOutlet weak var lbl_description: UILabel!
    @IBOutlet weak var lbl_with_prescription: UILabel!
    @IBOutlet weak var lbl_recomendations: UILabel!
    
    @IBOutlet weak var tv_subjects: UITableView!
    

    
    
    var vaccine = Vaccine()
    
    var array_subjects = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tv_subjects.backgroundColor = UIColor.white
        
        lbl_name.text = vaccine.name
        lbl_optional_name.text = vaccine.optional_name
        lbl_description.text = vaccine.descriptions!
        lbl_manufacturer.text = "FABRICANTE: " + vaccine.manufacturer!
        
        if vaccine.prescription_needed!
        {
            lbl_with_prescription.text = "VACUNA CON PRESCRIPCIÓN"
        }
        else
        {
            lbl_with_prescription.text = "VACUNA SIN PRESCRIPCIÓN"
        }

        lbl_recomendations.text = vaccine.indications
        
        let prevent_dic: NSMutableDictionary? = ["subject" : "LO QUE PREVIENE",
                                                 "subject_text" : vaccine.diseases_prevent ?? ""
        ]
        array_subjects.add(prevent_dic!)
        
        let contradiccions_dic: NSMutableDictionary? = ["subject" : "CONTRADICCIONES",
                                                 "subject_text" : vaccine.contradictions ?? ""
        ]
        array_subjects.add(contradiccions_dic!)
        
        let hecho_dic: NSMutableDictionary? = ["subject" : "¿DE QUE ESTA HECHO?",
                                               "subject_text" : vaccine.composition ?? ""
        ]
        array_subjects.add(hecho_dic!)
        
        let reactions_dic: NSMutableDictionary? = ["subject" : "POSIBLES REACCIONES",
                                                   "subject_text" : vaccine.posible_reactions ?? ""
        ]
        array_subjects.add(reactions_dic!)
        
        
        
        
        self.tv_subjects.reloadData()
    }
    
    @IBAction func close_action(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array_subjects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_vaccine_details", for: indexPath) as! VaccineDetailsCell
        
        let dict = array_subjects.object(at: indexPath.row) as! NSDictionary
        cell.lbl_subject.text = dict.object(forKey: "subject") as? String
        cell.lbl_subject_text.text = dict.object(forKey: "subject_text") as? String
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    

    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "segue_seemore_vaccine"
        {
            let vc : CalendarVC = segue.destination as! CalendarVC
            vc.vaccine = vaccine
            
        }
        
        
       
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
