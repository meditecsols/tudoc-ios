//
//  AddressVC.swift
//  BeehApp
//
//  Created by Arturo Escamilla on 04/03/21.
//

import UIKit
import GoogleMaps
import GooglePlaces


class AddressVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var txt_place: UITextField!
    
    @IBOutlet weak var tableView: UITableView!
    
    var resultsArray:[Dictionary<String, AnyObject>] = Array()
    
    var place_selected = NSDictionary()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.isHidden = true
        txt_place.addTarget(self, action: #selector(searchPlaceFromGoogle(_:)), for: .editingChanged)

        self.txt_place.adjustsFontSizeToFitWidth = true
        self.txt_place.minimumFontSize = 8.0
        txt_place.delegate = self
        
        
      
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        let address = GlobalMembers.preferences.object(forKey: GlobalMembers.address_key) as? NSDictionary
        if address != nil
        {
            self.performSegue(withIdentifier: "segue_menu", sender: nil)
        }
    }
    
    @objc func searchPlaceFromGoogle(_ textField:UITextField) {
        
        
     
     if let searchQuery = textField.text {
         var strGoogleApi = "https://maps.googleapis.com/maps/api/geocode/json?address=\(searchQuery)&key= AIzaSyCDCYksQOJyddd-C9IgWeSgksFW5MlWq0A"
         strGoogleApi = strGoogleApi.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
         
         var urlRequest = URLRequest(url: URL(string: strGoogleApi)!)
         urlRequest.httpMethod = "GET"
         let task = URLSession.shared.dataTask(with: urlRequest) { (data, resopnse, error) in
             if error == nil {
                 
                 if let responseData = data {
                 let jsonDict = try? JSONSerialization.jsonObject(with: responseData, options: .mutableContainers)
                     
                     if let dict = jsonDict as? Dictionary<String, AnyObject>{
                         
                         if let results = dict["results"] as? [Dictionary<String, AnyObject>] {
                             print("json == \(results)")
                             self.resultsArray.removeAll()
                             for dct in results {
                                 self.resultsArray.append(dct)
                             }
                             
                             DispatchQueue.main.async {
                                if self.resultsArray.count > 0
                                {
                                    self.tableView.isHidden = false;
                                }
                                else
                                {
                                    self.tableView.isHidden = true;
                                }
                              self.tableView.reloadData()
                             }
                             
                         }
                     }
                    
                 }
             } else {
                 //we have error connection google api
             }
         }
         task.resume()
     }
     }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resultsArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        //var section = indexPath.section

        //var row = indexPath.row

        let cell: UITableViewCell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier:"addCategoryCell")

        //cell.selectionStyle =  UITableViewCell.SelectionStyle.none
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        cell.textLabel?.textAlignment = NSTextAlignment.left
        cell.textLabel?.textColor = UIColor.black
        cell.textLabel?.font = UIFont.systemFont(ofSize: 14.0)
        

        
        let place = self.resultsArray[indexPath.row]
        cell.textLabel?.text = "\(place["formatted_address"] as! String)"

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let place = self.resultsArray[indexPath.row] as NSDictionary
        let address_components = place.value(forKey: "address_components") as! NSMutableArray
        //print(address_components)
        
        for component in address_components
        {
            let comp = component as! NSDictionary
            let types = comp.value(forKey: "types") as! NSArray
            
           if( self.detectPostal(type: types))
           {
            
            GlobalMembers.preferences.removeObject(forKey: GlobalMembers.address_key)
            //let address = GlobalMembers.preferences.object(forKey: GlobalMembers.address_key) as? NSDictionary
            self.tableView.isHidden = true
            txt_place.text = place.object(forKey: "formatted_address") as? String
            place_selected = place
            txt_place.resignFirstResponder()
           }
            
        }
        
    }
    
    func detectPostal(type:NSArray) -> Bool {
        
        for item in type {
            if(item as! String == "postal_code")
            {
                return true
            }
            
        }
        
        return false
    }


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
   /* override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        

    }*/
    
    
    @IBAction func next_action(_ sender: Any) {
        
        if txt_place.text == ""
        {
        
            AlertManager.alert(title: "Alerta",mess: "Ingresa una dirección",viewcontroller: self)
          
            return
        }
        
        GlobalMembers.preferences.set(place_selected, forKey: GlobalMembers.address_key)
        self.performSegue(withIdentifier: "segue_menu", sender: nil)
        
    }
    
    

}

@IBDesignable extension UIView {

    @IBInspectable var shadow: Bool {
        get {
            return layer.shadowOpacity > 0.0
        }
        set {
            if newValue == true {
                self.addShadow()
            }
        }
    }

    @IBInspectable var cornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set {
            self.layer.cornerRadius = newValue

            // Don't touch the masksToBound property if a shadow is needed in addition to the cornerRadius
            if shadow == false {
                self.layer.masksToBounds = true
            }
        }
    }


    func addShadow(shadowColor: CGColor = UIColor.black.cgColor,
               shadowOffset: CGSize = CGSize(width: 1.0, height: 2.0),
               shadowOpacity: Float = 0.4,
               shadowRadius: CGFloat = 3.0) {
        layer.shadowColor = shadowColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
    }
}


@IBDesignable extension UIButton {

    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }

    @IBInspectable override var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }

    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
}

@IBDesignable extension UITextField {

    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }

    @IBInspectable override var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }

    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
}

@IBDesignable
extension UITextField {

    @IBInspectable var paddingLeftCustom: CGFloat {
        get {
            return leftView!.frame.size.width
        }
        set {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
            leftView = paddingView
            leftViewMode = .always
        }
    }

    @IBInspectable var paddingRightCustom: CGFloat {
        get {
            return rightView!.frame.size.width
        }
        set {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
            rightView = paddingView
            rightViewMode = .always
        }
    }
}
