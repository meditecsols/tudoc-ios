//
//  SeeMoreExamVC.swift
//  BeehApp
//
//  Created by Arturo Escamilla on 05/04/21.
//

import UIKit

class ExamDetailsCell:UITableViewCell
{
    @IBOutlet weak var lbl_subject: UILabel!
    
    @IBOutlet weak var lbl_subject_text: UILabel!
}

class SeeMoreExamVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_optional_name: UILabel!
    @IBOutlet weak var lbl_manufacturer: UILabel!
    @IBOutlet weak var lbl_description: UILabel!
    @IBOutlet weak var lbl_with_prescription: UILabel!
    @IBOutlet weak var lbl_recomendations: UILabel!
    
    @IBOutlet weak var tv_subjects: UITableView!
    

    
    var exam = Exam()
    
    var array_subjects = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()

        tv_subjects.backgroundColor = UIColor.white
        lbl_name.text = exam.name
        lbl_optional_name.text = exam.medical_name
        lbl_description.text = exam.descriptions!
        lbl_manufacturer.isHidden = true
        lbl_with_prescription.isHidden = true

        lbl_recomendations.text = exam.contraindications
        
        let prevent_dic: NSMutableDictionary? = ["subject" : "EN DONDE",
                                                 "subject_text" : exam.when ?? ""
        ]
        array_subjects.add(prevent_dic!)
        
        let contradiccions_dic: NSMutableDictionary? = ["subject" : "CONTRAINDICACIONES",
                                                 "subject_text" : exam.contraindications ?? ""
        ]
        array_subjects.add(contradiccions_dic!)
        
        let hecho_dic: NSMutableDictionary? = ["subject" : "¿COMÓ SE APLÍCA?",
                                               "subject_text" : exam.how ?? ""
        ]
        array_subjects.add(hecho_dic!)
        
        let reactions_dic: NSMutableDictionary? = ["subject" : "¿QUE ES?",
                                                   "subject_text" : exam.what_is ?? ""
        ]
        array_subjects.add(reactions_dic!)
        
        let when_result_dic: NSMutableDictionary? = ["subject" : "¿CUANDO SE DA EL RESULTADO?",
                                                   "subject_text" : exam.when_result ?? ""
        ]
        array_subjects.add(when_result_dic!)
        
        
        
        
        self.tv_subjects.reloadData()
        // Do any additional setup after loading the view.
    }
    

    @IBAction func close_action(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array_subjects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_exam_details", for: indexPath) as! ExamDetailsCell
        
        let dict = array_subjects.object(at: indexPath.row) as! NSDictionary
        cell.lbl_subject.text = dict.object(forKey: "subject") as? String
        cell.lbl_subject_text.text = dict.object(forKey: "subject_text") as? String
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //let dict = array_subjects.object(at: indexPath.row) as! NSDictionary
       // performSegue(withIdentifier: "segue_payment_exam", sender: nil)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
