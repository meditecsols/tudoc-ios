//
//  VaccineVC.swift
//  BeehApp
//
//  Created by Arturo Escamilla on 10/03/21.
//

import UIKit
import SwiftSpinner
class VaccineCell : UITableViewCell
{
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_description: UILabel!
    @IBOutlet weak var lbl_price: UILabel!
    
    
}


let invokeService = InvokeService()


class VaccineVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var btn_address: UIButton!
    
    @IBOutlet weak var tv_vaccines: UITableView!
    
    @IBOutlet weak var lbl_message: UILabel!
    
    var zip_code = String()
    
    var vaccine_array = NSMutableArray()
    
    var selected_vaccine = Int()
    
    var refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.tv_vaccines.isHidden = true
        self.lbl_message.isHidden = false
        tv_vaccines.backgroundColor = UIColor.white
        
//        let place = GlobalMembers.preferences.object(forKey: GlobalMembers.address_key) as? NSDictionary
//        
//        // getZipCode
//        
//        zip_code = GlobalMethods.getZipCode(place: place!)
//        
//        // Set address to button title
//        let format_address = GlobalMethods.getFormatedAddress(place: place!)
//        
//        btn_address.setTitle(format_address, for: .normal)
        
        //self.getVaccines()
        
        self.refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tv_vaccines.addSubview(refreshControl)

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        
        self.getVaccines()
    }
    
    @objc func refresh(_ sender: AnyObject) {
        getVaccines()
    }
    
    func getVaccines() {
        SwiftSpinner.show("")
        invokeService.requestVaccinesDataWith(zipcode: zip_code) { (response, error, code) in
            
            if code == 200
            {
                DispatchQueue.main.async
                { [self] in
                    SwiftSpinner.hide()
                    self.refreshControl.endRefreshing()
                    self.vaccine_array = NSMutableArray()
                    if response!.count > 0
                    {
                        //self.tv_vaccines.isHidden = false
                        self.lbl_message.isHidden = true
                        
                        self.vaccine_array = self.filterData(vaccines: response!)
                    }
                    else
                    {
                        //self.tv_vaccines.isHidden = true
                        self.lbl_message.isHidden = false
                    
                    }
                    
                    
                    
                    self.tv_vaccines.reloadData()
                }
                
                
            }
            else
            {
                DispatchQueue.main.async {
                    SwiftSpinner.hide()
                }
            }
        }
        
    }
    
    func filterData(vaccines:NSArray) -> NSMutableArray {
        
        let array_result = NSMutableArray()
        
        for vacc in vaccines
        {
            //let vac_entitie = VaccineCode()
            let vaccine_entitie = Vaccine()
            
            let vaccine = vacc as! NSDictionary
            
            //vac_entitie.id = item.object(forKey: "id") as? Int
            //vac_entitie.created_on = item.object(forKey: "created_on") as? String
            
            //let vaccine = item.object(forKey: "vaccine") as? NSDictionary
            
            vaccine_entitie.id = vaccine.object(forKey: "id") as? Int
            vaccine_entitie.name = vaccine.object(forKey: "name") as? String
            vaccine_entitie.optional_name = vaccine.object(forKey: "optional_name") as? String
            vaccine_entitie.manufacturer = vaccine.object(forKey: "manufacturer") as? String
            vaccine_entitie.prescription_needed = vaccine.object(forKey: "prescription_needed") as? Bool
            vaccine_entitie.indications = vaccine.object(forKey: "indications") as? String
            vaccine_entitie.dosage_scheme = vaccine.object(forKey: "dosage_scheme") as? String
            vaccine_entitie.diseases_prevent = vaccine.object(forKey: "diseases_prevent") as? String
            vaccine_entitie.contradictions = vaccine.object(forKey: "contradictions") as? String
            vaccine_entitie.composition = vaccine.object(forKey: "composition") as? String
            vaccine_entitie.posible_reactions = vaccine.object(forKey: "posible_reactions") as? String
            vaccine_entitie.descriptions = vaccine.object(forKey: "description") as? String
            vaccine_entitie.vaccine_type = vaccine.object(forKey: "vaccine_type") as? Int
            vaccine_entitie.cost = Double((vaccine.object(forKey: "cost") as? String)!)
            vaccine_entitie.price = Double((vaccine.object(forKey: "price") as? String)!)
            
            vaccine_entitie.sku = vaccine.object(forKey: "sku") as? String

            vaccine_entitie.photo = vaccine.object(forKey: "photo") as? String
            vaccine_entitie.is_active = vaccine.object(forKey: "is_active") as? Bool
            vaccine_entitie.created_on = vaccine.object(forKey: "created_on") as? String
            
            
            //vac_entitie.vaccine = vaccine_entitie
            
            //vac_entitie.postal_code = item.object(forKey: "postal_code") as? Int
            
            array_result.add(vaccine_entitie)
            
            
            
        }
        
        return array_result
        
    }
    
    @IBAction func edit_address(_ sender: Any) {
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return vaccine_array.count
    }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_vaccine", for: indexPath) as! VaccineCell
        let vaccine_dic = vaccine_array.object(at: indexPath.row) as! Vaccine
        
        cell.lbl_name.text = vaccine_dic.name
        cell.lbl_description.text = vaccine_dic.descriptions
        cell.lbl_price.text = GlobalMethods.convertDoubleToCurrency(amount: (vaccine_dic.price)!)
        

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selected_vaccine = indexPath.row
        performSegue(withIdentifier: "segue_vac_details", sender: nil)
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let vaccine = vaccine_array[selected_vaccine] as! Vaccine
      
        let vc : VaccineDetailsVC = segue.destination as! VaccineDetailsVC
        vc.vaccine = vaccine
        
    }
    

}
