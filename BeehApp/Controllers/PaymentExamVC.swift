//
//  PaymentExamVC.swift
//  BeehApp
//
//  Created by Arturo Escamilla on 04/04/21.
//

import UIKit
import SwiftSpinner
class PaymentExamVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_address: UILabel!
    @IBOutlet weak var lbl_date: UILabel!
    @IBOutlet weak var lbl_price: UILabel!
    @IBOutlet weak var txt_number_card: UITextField!
    @IBOutlet weak var txt_name_card: UITextField!
    @IBOutlet weak var txt_month_card: UITextField!
    @IBOutlet weak var txt_year_card: UITextField!
    @IBOutlet weak var txt_cvc_card: UITextField!
    

    var exam = Exam()
    var institution = Institution()
    var date = String()
    var quantity = Int()
    var insite = Bool()
    var selected_hour = NSArray()


    override func viewDidLoad() {
        super.viewDidLoad()
        
        txt_number_card.delegate = self
        
        lbl_name.text = "(" + String(quantity) + ") " + exam.name!
        lbl_date.text = "Fecha: " + date
        lbl_address.text = institution.name
        lbl_price.text = "Total: " + GlobalMethods.convertDoubleToCurrency(amount: exam.price! * Double(quantity))

        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))


       view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    @IBAction func action_pay(_ sender: Any) {
        
        if txt_number_card.text == ""
        {
            AlertManager.alert(title: "TuDoc", mess: "Número inválido", viewcontroller: self)
            return
        }
        
        if txt_name_card.text == ""
        {
            AlertManager.alert(title: "TuDoc", mess: "Nombre inválido", viewcontroller: self)
            return
        }
        
        if txt_month_card.text == "" || txt_year_card.text == ""
        {
            AlertManager.alert(title: "TuDoc", mess: "Fecha inválida", viewcontroller: self)
            return
        }
        
        if txt_cvc_card.text == ""
        {
            AlertManager.alert(title: "TuDoc", mess: "CVC inválido", viewcontroller: self)
            return
        }
        
        let conekta = Conekta()
        conekta.delegate = self
        conekta.publicKey = Config.CONEKTA_KEY
        conekta.collectDevice()
        
        let card = conekta.card()
        
        card?.setNumber(txt_number_card.text, name: txt_name_card.text, cvc: txt_cvc_card.text , expMonth: txt_month_card.text, expYear: txt_year_card.text)
        
        let token = conekta.token()
        token?.card = card
        
        token?.create(success: { [self] (data) -> Void in
            if let data = data as NSDictionary? as! [String:Any]? {
                
                if data.count == 4
                {
                    let token_ui = data["id"] as? String
                    
                    var amount = exam.price
                    amount = amount! * 100;
                    
                    let dic_link = NSMutableDictionary()
                    dic_link.setValue(token_ui, forKey: "token_card")
                    //dic_link.setValue("tok_test_visa_4242", forKey: "token_card")
                    dic_link.setValue(NSNumber.init(value: exam.id!), forKey: "exam_id")
                    dic_link.setValue(NSNumber.init(value: institution.id!), forKey: "institution_id")
                    dic_link.setValue(NSNumber.init(value: quantity), forKey: "quanity")
                    dic_link.setValue(NSNumber.init(value: amount!), forKey: "amount")
                    dic_link.setValue(NSNumber.init(booleanLiteral: true), forKey: "in_site")
                    dic_link.setValue(selected_hour[0], forKey: "start_time")
                    dic_link.setValue(selected_hour[1], forKey: "end_time")
                    
                    let new_service_arr = NSMutableArray()
                    let dic_item = NSMutableDictionary()
                    
                    dic_item.setValue(exam.name, forKey: "name")
                    dic_item.setValue(exam.descriptions, forKey: "description")
                    var unit_price = exam.price
                    unit_price = unit_price! * 100
                    dic_item.setValue(NSNumber.init(value: unit_price!), forKey: "unit_price")
                    dic_item.setValue(NSNumber.init(value: 1), forKey: "quantity")
                    dic_item.setValue("exam123", forKey: "sku")
                    dic_item.setValue("examenes", forKey: "category")
                    dic_item.setValue("physical", forKey: "type")
                    let tags_array = ["TuDoc"]
                    dic_item.setValue(tags_array, forKey: "tags")
                    
                    
                    new_service_arr.add(dic_item)
                    dic_link.setValue(new_service_arr, forKey: "line_items")
                    //print(dic_link)
                    SwiftSpinner.show("")
                    invokeService.requestForChargeExamDataWith(dic_link) { (response, error, code) in
                        SwiftSpinner.hide()
                        if code == 200
                        {
                            DispatchQueue.main.async {
                                AlertManager.alert(title: "TuDoc", mess: "Se ha registrado tu orden!", viewcontroller: self)
                                self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
                            }
                            
                        }
                        else{
                            DispatchQueue.main.async {
                                let message = response?.object(forKey: "message") as! String
                                AlertManager.alert(title: "TuDoc", mess: message, viewcontroller: self)
                            }
                        }
                    }

                }
                else
                {
                    let message = data["message"] as? String
                    
                    AlertManager.alert(title: "TuDoc", mess: message!, viewcontroller: self)
                }
            }
        }, andError: { (error) -> Void in
            //print(error)
        })
        
        
    }
    
    @IBAction func action_close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if txt_number_card == textField
        {
            // get the current text, or use an empty string if that failed
            let currentText = textField.text ?? ""

            // attempt to read the range they are trying to change, or exit if we can't
            guard let stringRange = Range(range, in: currentText) else { return false }

            // add their new text to the existing text
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)

            // make sure the result is under 16 characters
            return updatedText.count <= 16
        }
        
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
