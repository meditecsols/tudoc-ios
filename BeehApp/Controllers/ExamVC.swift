//
//  ExamVC.swift
//  BeehApp
//
//  Created by Arturo Escamilla on 10/03/21.
//

import UIKit
import SwiftSpinner
class ExamCell : UITableViewCell
{
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_description: UILabel!
    @IBOutlet weak var lbl_price: UILabel!
    
    
}




class ExamVC: UIViewController ,UITableViewDataSource,UITableViewDelegate {
    
    let invokeService = InvokeService()
    
    @IBOutlet weak var btn_address: UIButton!
    
    @IBOutlet weak var tv_exams: UITableView!
    
    @IBOutlet weak var lbl_message: UILabel!
    
    var zip_code = String()
    
    var exam_array = NSMutableArray()
    
    var selected_exam = Int()
    
    var refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tv_exams.isHidden = true
        self.lbl_message.isHidden = false
        tv_exams.backgroundColor = UIColor.white
//        let place = GlobalMembers.preferences.object(forKey: GlobalMembers.address_key) as? NSDictionary
//        
//        // getZipCode
//        
//        zip_code = GlobalMethods.getZipCode(place: place!)
//        
//        // Set address to button title
//        let format_address = GlobalMethods.getFormatedAddress(place: place!)
//        
//        btn_address.setTitle(format_address, for: .normal)
        
        //self.getExams()
        
        self.refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tv_exams.addSubview(refreshControl)

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        
        self.getExams()
    }
    
    @objc func refresh(_ sender: AnyObject) {
        self.getExams()
    }
    
    func getExams() {
        SwiftSpinner.show("")
        invokeService.requestExamsDataWith(zipcode: zip_code) { (response, error, code) in
            
            if code == 200
            {
                DispatchQueue.main.async
                { [self] in
                    SwiftSpinner.hide()
                    self.refreshControl.endRefreshing()
                    self.exam_array = NSMutableArray()
                    if response!.count > 0
                    {
                        self.tv_exams.isHidden = false
                        self.lbl_message.isHidden = true
                        
                        self.exam_array = self.filterData(exams: response!)
                    }
                    else
                    {
                        self.tv_exams.isHidden = true
                        self.lbl_message.isHidden = false
                    
                    }
                    
                    
                    
                    self.tv_exams.reloadData()
                }
                
                
            }
            else
            {
                DispatchQueue.main.async {
                    SwiftSpinner.hide()
                }
            }
        }
        
    }
    
    func filterData(exams:NSArray) -> NSMutableArray {
        
        let array_result = NSMutableArray()
        
        for exam in exams
        {
            //let exam_order_entitie = ExamCode()
            let exam_entitie = Exam()
            
            let exam = exam as! NSDictionary
            
            //exam_order_entitie.id = item.object(forKey: "id") as? Int
            //exam_order_entitie.created_on = item.object(forKey: "created_on") as? String
            
            //let exam = item.object(forKey: "exam") as? NSDictionary
            
            exam_entitie.id = exam.object(forKey: "id") as? Int
            exam_entitie.name = exam.object(forKey: "name") as? String
            exam_entitie.medical_name = exam.object(forKey: "medical_name") as? String
            exam_entitie.descriptions = exam.object(forKey: "description") as? String
            exam_entitie.recomendations = exam.object(forKey: "recomendations") as? String
            exam_entitie.contraindications = exam.object(forKey: "contraindications") as? String
            exam_entitie.when = exam.object(forKey: "when") as? String
            exam_entitie.how = exam.object(forKey: "how") as? String
            exam_entitie.what_is = exam.object(forKey: "what_is") as? String
            exam_entitie.when_result = exam.object(forKey: "when_result") as? String
            exam_entitie.exam_type = exam.object(forKey: "exam_type") as? Int
            exam_entitie.cost = Double((exam.object(forKey: "cost") as? String)!)
            exam_entitie.price = Double((exam.object(forKey: "price") as? String)!)
            exam_entitie.photo = exam.object(forKey: "photo") as? String
            exam_entitie.is_active = exam.object(forKey: "is_active") as? Bool
            exam_entitie.created_on = exam.object(forKey: "created_on") as? String
            
            //exam_order_entitie.exam = exam_entitie
            
            //exam_order_entitie.postal_code = item.object(forKey: "postal_code") as? Int
            
            array_result.add(exam_entitie)
            
            
            
        }
        
        return array_result
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return exam_array.count
    }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_exam", for: indexPath) as! ExamCell
        let exam_dic = exam_array.object(at: indexPath.row) as! Exam
        
        cell.lbl_name.text = exam_dic.name
        cell.lbl_description.text = exam_dic.descriptions
        cell.lbl_price.text = GlobalMethods.convertDoubleToCurrency(amount: (exam_dic.price)!)
        

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selected_exam = indexPath.row
        performSegue(withIdentifier: "segue_exam_details", sender: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let exam = exam_array[selected_exam] as! Exam
      
        let vc : ExamDetailsVC = segue.destination as! ExamDetailsVC
        vc.exam = exam
        
    }

}
