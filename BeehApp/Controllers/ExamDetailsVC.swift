//
//  ExamDetailsVC.swift
//  BeehApp
//
//  Created by Arturo Escamilla on 04/04/21.
//

import UIKit
import SwiftSpinner


class ExamDetailsVC: UIViewController {
    
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_optional_name: UILabel!
    @IBOutlet weak var lbl_manufacturer: UILabel!

    
    @IBOutlet weak var lbl_amount: UILabel!
    
    @IBOutlet weak var stepper_amount: UIStepper!
    
    @IBOutlet weak var lbl_price: UILabel!
    
    
    var exam = Exam()
    
    var array_subjects = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        lbl_price.text = GlobalMethods.convertDoubleToCurrency(amount: exam.price! * Double(1.0))
        
        lbl_name.text = exam.name
        lbl_optional_name.text = exam.medical_name
        lbl_manufacturer.text = exam.descriptions
        

        
    }
    
    @IBAction func close_action(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    

    
    @IBAction func stepper_value_changed(_ sender: Any) {
        let int_value = Int((sender as! UIStepper).value)
        
        lbl_price.text = GlobalMethods.convertDoubleToCurrency(amount: exam.price! * Double(int_value))
        lbl_amount.text = String(int_value)
        
    }
    @IBAction func btn_schedule(_ sender: Any) {
        
        performSegue(withIdentifier: "segue_calendar_exam", sender: nil)
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "segue_calendar_exam"
        {
            let vc : CalendarExamVC = segue.destination as! CalendarExamVC
            vc.exam = exam
            let amount = lbl_amount.text
            vc.quantity = Int(amount!)!
        }
        
        if segue.identifier == "segue_seemore_exam"
        {
            let vc : SeeMoreExamVC = segue.destination as! SeeMoreExamVC
            vc.exam = exam
            
        }
        
       
    }
   
    @IBAction func action_seemore_exam(_ sender: Any) {
        performSegue(withIdentifier: "segue_seemore_exam", sender: nil)
    }
    


}
