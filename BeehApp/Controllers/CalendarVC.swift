//
//  CalendarVC.swift
//  BeehApp
//
//  Created by Arturo Escamilla on 17/03/21.
//

import UIKit
import SwiftSpinner
class HoursCell:UITableViewCell
{
    
    @IBOutlet weak var lbl_hour: UILabel!
    
}

class CalendarVC: UIViewController,FSCalendarDataSource, FSCalendarDelegate, FSCalendarDelegateAppearance,UITableViewDelegate,UITableViewDataSource,SendInstitutionsVCDelegate {

    
    
    
    
    @IBOutlet weak var calendar: FSCalendar!
    fileprivate let gregorian: Calendar = Calendar(identifier: .gregorian)
    
    @IBOutlet weak var calendarHeightConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var tv_hours: UITableView!
    
    @IBOutlet weak var view_hours: UIView!
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter
    }()
    fileprivate lazy var dateFormatter1: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter
    }()
    
    let borderDefaultColors = NSMutableArray()
    var vaccine = Vaccine()
    var invokeServ = InvokeService()
    let alert = AlertManager()
    var workwindow_array = NSMutableArray()
    
    var hours_array = NSMutableArray()
    var selected_hour = NSArray()
    
    
    var date_selected = String()
    var date_complete_selected = String()
    var cart = ShoppingVaccineCart()
    
    var quantity = Int()

    override func viewDidLoad() {
        super.viewDidLoad()
        tv_hours.isHidden = true
        tv_hours.backgroundColor = UIColor.white
        self.view_hours.isHidden = true
        if UIDevice.current.model.hasPrefix("iPad") {
            self.calendarHeightConstraint.constant = 400
        }
        
        self.calendar.select(Date())
        //calendar.allowsMultipleSelection = true
        calendar.appearance.caseOptions = [.headerUsesUpperCase,.weekdayUsesSingleUpperCase]
        
        
       // self.view.addGestureRecognizer(self.scopeGesture)
        //self.tableView.panGestureRecognizer.require(toFail: self.scopeGesture)
        self.calendar.scope = .month
        
        // For UITest
        self.calendar.accessibilityIdentifier = "calendar"

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.calendar.reloadData()
        
        if let monthInt = Calendar.current.dateComponents([.month], from: Date()).month {
            print(monthInt) // 4
            workingDays(month: monthInt)
        }
        
        
    }
    
    func workingDays(month : Int) {
        SwiftSpinner.show("")
        invokeServ.requestVaccineWindowWorkingDataWith(id: String(vaccine.id!)) { (response, error, code) in
            
            if(code == 200)
            {
                for item in response! {
                    
                    let dic = item as! NSDictionary
                    let workwindow = Workwindow()
                    workwindow.id = dic.object(forKey: "id") as? Int
                    workwindow.working_day = dic.object(forKey: "working_day") as? Bool
                    workwindow.start_time = dic.object(forKey: "start_time") as? String
                    workwindow.end_time = dic.object(forKey: "end_time") as? String
                    workwindow.vaccine = dic.object(forKey: "vaccine") as? Int
                    workwindow.days = dic.object(forKey: "days") as? Array
                    self.workwindow_array.add(workwindow)
                    
                }
                
                let allDays = Date().getAllDays(month: month)
                
                for item in allDays {
                    let date = item as Date
                    let calendar = Calendar.current
                    let dateComponents = calendar.dateComponents([.weekday], from: date)
                    let weekday = dateComponents.weekday
                    let odmday = self.odmday(day: weekday!)
                    let workwindow = self.workwindow_array[0] as! Workwindow

                        
                    if(workwindow.days!.contains(odmday))
                    {
                            
                            let key = self.dateFormatter.string(from: date)
                            self.borderDefaultColors.add(key)
                    }
                        
                    
                    
                    DispatchQueue.main.async {
                        SwiftSpinner.hide()
                        self.calendar.reloadData()
                    }
                  

                }
            }
            else
            {
                DispatchQueue.main.async {
                    SwiftSpinner.hide()
                }
            }
            
        }
    }
    
    func odmday(day:Int) -> Int {
        
        switch day {
        case 1:
            return 7
        case 2:
            return 1
        case 3:
            return 2
        case 4:
            return 3
        case 5:
            return 4
        case 6:
            return 5
        case 7:
            return 6
        default:
            return 0
        }
    }



    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.calendarHeightConstraint.constant = bounds.height
        self.view.layoutIfNeeded()
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        //print("did select date \(self.dateFormatter.string(from: date))")
        
        let selectedDates = calendar.selectedDates.map({self.dateFormatter.string(from: $0)})

        let sel_date = self.convertStringToDate(date_string: selectedDates[0])
    
        if sel_date < Date() {
            self.tv_hours.isHidden = true
            AlertManager.alert(title: "TuDoc", mess: "En esta fecha ya no se puede elegir!", viewcontroller: self)
           
            
           
            return
        }
        let selected = selectedDates[0]
        var selectedDate = selected.replacingOccurrences(of: "/", with: "-", options: .literal, range: nil)
        date_selected = selectedDate
        selectedDate = selectedDate + "T00:00:00"
        
        let key = self.dateFormatter.string(from: date)
        if borderDefaultColors.contains(key) {
           
            hours_array = NSMutableArray()
            SwiftSpinner.show("")
            invokeServ.requestAvailabilityVaccineDataWith(id: String(vaccine.id!), date: selectedDate, slotsz: "30") { (response, error, code) in
                SwiftSpinner.hide()
                if(code == 200)
                {
                    if(response!.count>0)
                    {
                        DispatchQueue.main.async {
                            self.tv_hours.isHidden = false
                            self.view_hours.isHidden = false
                            self.hours_array = NSMutableArray(array:response!)
                            self.tv_hours.reloadData()
//                        self.delegate?.resfreshdate(date: selected,response: response!)
//                        self.dismiss(animated: true)
                        }
                        
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            self.tv_hours.reloadData()
                            self.tv_hours.isHidden = true
                            self.view_hours.isHidden = true
                            AlertManager.alert(title: "TuDoc", mess: "No hay horario disponible", viewcontroller: self)
                        }
                        
                    }
                }
            }
            //self.dismiss(animated: true, completion: nil)
        }
        
        //print("selected dates is \(selectedDates)")
        if monthPosition == .next || monthPosition == .previous {
            calendar.setCurrentPage(date, animated: true)
        }
    }

    func convertStringToDate(date_string:String) -> Date {
        let dateString = date_string
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        return dateFormatter.date(from: dateString)!
    }

    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        print("\(self.dateFormatter.string(from: calendar.currentPage))")
        let month = Calendar.current.component(.month, from: calendar.currentPage)

        print(month)
        
        workingDays(month: month)
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, borderDefaultColorFor date: Date) -> UIColor? {
        let key = self.dateFormatter.string(from: date)
        if borderDefaultColors.contains(key) {
            
            return UIColor.gray
        }
        return appearance.borderDefaultColor
    }
    

    

    
    @IBAction func btn_back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hours_array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_hours", for: indexPath) as! HoursCell
        let hours = hours_array.object(at: indexPath.row) as! NSArray
        let start_time = GlobalMethods.setHourService(date: hours[0] as! String)
        cell.lbl_hour.text = start_time
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selected_hour = NSArray()
        let hours = hours_array.object(at: indexPath.row) as! NSArray
        selected_hour = hours
        let start_time = GlobalMethods.setHourService(date: hours[0] as! String)
        date_complete_selected = ""
        date_complete_selected = date_selected + " " + start_time
        
        let token = GlobalMembers.preferences.object(forKey: GlobalMembers.tokenKey) as? String
        if token != nil
        {
            self.getPatient()
            
            
        }
        else
        {
            performSegue(withIdentifier: "login_segue", sender: nil)
            
        }
        
        
        //cart.productList.append((vaccine, cantidad, product.price * cantidad))
        
    }
    
    func getPatient()
    {
        SwiftSpinner.show("")
        invokeServ.requestPatientDataWith { (response, error, code) in
            SwiftSpinner.hide()
            if code == 200
            {
                DispatchQueue.main.async {
                    
                    self.performSegue(withIdentifier: "segue_institutions", sender: nil)
                   
                }
            }
            else
            {
                self.invokeServ.refreshTokenDataWith { (response, error) in
                    if response!
                    {
                        self.getPatient()
                    }
                }

            }
        }
    }
   
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "segue_institutions"
        {
            let vc : InstitutionsVC = segue.destination as! InstitutionsVC
            vc.vaccine = vaccine
            vc.date = date_complete_selected
            vc.quantity = quantity
            vc.selected_hour = selected_hour
            vc.fromVaccine = true
        }
        
        if segue.identifier == "login_segue"
        {
            let vc : LoginVC = segue.destination as! LoginVC
            vc.vaccine = vaccine
            vc.isfromVaccine = true
            vc.selected_hour = selected_hour
            vc.date = date_complete_selected
            vc.quantity = quantity
            vc.delegateLV = self
        }
       
    }
    
    func seguetoInstitutions() {
        
        self.performSegue(withIdentifier: "segue_institutions", sender: nil)
        
    }
    
}


extension Date
{
    mutating func addDays(n: Int)
    {
        let cal = Calendar.current
        self = cal.date(byAdding: .day, value: n, to: self)!
    }

    func firstDayOfTheMonth(date:Date) -> Date {
        return Calendar.current.date(from:
            Calendar.current.dateComponents([.year,.month], from: date))!
    }

    func getAllDays(month:Int) -> [Date]
    {
        var days = [Date]()

        let calendar = Calendar.current
    
        var dateComponents = DateComponents(year: calendar.component(.year, from: self), month: calendar.component(.month, from: self))
        dateComponents.month = month
        let date = calendar.date(from: dateComponents)!
        print(date)
        let range = calendar.range(of: .day, in: .month, for: date)!

        var day = firstDayOfTheMonth(date: date)

        for _ in 1...range.count
        {
            days.append(day)
            day.addDays(n: 1)
        }

        return days
    }
    
  
}

