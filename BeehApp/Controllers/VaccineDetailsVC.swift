//
//  VaccineDetailsVC.swift
//  BeehApp
//
//  Created by Arturo Escamilla on 16/03/21.
//

import UIKit
import SwiftSpinner


class VaccineDetailsVC: UIViewController {
    
    
    
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_optional_name: UILabel!
    @IBOutlet weak var lbl_manufacturer: UILabel!
  
    
    @IBOutlet weak var lbl_amount: UILabel!
    
    @IBOutlet weak var stepper_amount: UIStepper!
    
    @IBOutlet weak var lbl_price: UILabel!
    
    
    var vaccine = Vaccine()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lbl_price.text = GlobalMethods.convertDoubleToCurrency(amount: vaccine.price! * Double(1.0)) 
        
        lbl_name.text = vaccine.name
        lbl_optional_name.text = vaccine.optional_name
        lbl_manufacturer.text = vaccine.descriptions!
        

        // Do any additional setup after loading the view.
    }
    
    @IBAction func close_action(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }

    
    @IBAction func stepper_value_changed(_ sender: Any) {
        let int_value = Int((sender as! UIStepper).value)
        
        lbl_price.text = GlobalMethods.convertDoubleToCurrency(amount: vaccine.price! * Double(int_value))
        lbl_amount.text = String(int_value)
        
    }
    @IBAction func btn_schedule(_ sender: Any) {
        
        performSegue(withIdentifier: "segue_calendar_vaccine", sender: nil)
    }
    
    @IBAction func action_see_more(_ sender: Any) {
        performSegue(withIdentifier: "segue_seemore_vaccine", sender: nil)

    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "segue_calendar_vaccine"
        {
            let vc : CalendarVC = segue.destination as! CalendarVC
            vc.vaccine = vaccine
            let amount = lbl_amount.text
            vc.quantity = Int(amount!)!
        }
        if segue.identifier == "segue_seemore_vaccine"
        {
            let vc : SeeMoreVaccineVC = segue.destination as! SeeMoreVaccineVC
            vc.vaccine = vaccine
        }
       
    }
   
    
}



