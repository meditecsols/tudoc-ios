//
//  LoginVC.swift
//  BeehApp
//
//  Created by Arturo Escamilla on 31/03/21.
//

import UIKit
import SwiftSpinner

protocol OptionsVCDelegate {
    func refresh()
}

protocol SendInstitutionsVCDelegate {
    func seguetoInstitutions()
}


class LoginVC: UIViewController,RegisterVCDelegate {
    
    
    
    var delegate: OptionsVCDelegate?
    var delegateLV : SendInstitutionsVCDelegate?
    
    @IBOutlet weak var txt_user: UITextField!
    @IBOutlet weak var txt_password: UITextField!
    
    let invokeServ = InvokeService()
    
    var vaccine = Vaccine()
    var exam = Exam()
    var date = String()
    var quantity = Int()
    var selected_hour = NSArray()
    var isfromVaccine = Bool()
    var fromMenu = Bool()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
                
               //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
               //tap.cancelsTouchesInView = false

               view.addGestureRecognizer(tap)
               
               observeKeyboardNotifications()
               
               txt_user.attributedPlaceholder = NSAttributedString(string:"Usuario", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
               txt_password.attributedPlaceholder = NSAttributedString(string:"Contraseña", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])

        // Do any additional setup after loading the view.
    }
    
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    fileprivate func observeKeyboardNotifications()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardShow), name:UIResponder.keyboardWillShowNotification , object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide), name:UIResponder.keyboardWillHideNotification , object: nil)
        
    }
    @objc func keyboardHide()
    {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.view.frame = CGRect(x: 0, y:0, width: self.view.frame.width, height: self.view.frame.height)
        }, completion: nil)
        
    }

    @objc func keyboardShow()
    {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.view.frame = CGRect(x: 0, y: -150, width: self.view.frame.width, height: self.view.frame.height)
        }, completion: nil)
    
    }
    
    @IBAction func btn_signin(_ sender: Any) {
        
        if(txt_user.text?.count == 0)
        {
            AlertManager.alert(title: "TuDoc", mess: "Ingresa un correo", viewcontroller: self)
            return
        }
        else
        {
            if !isValidEmail(emailStr: txt_user.text!)
            {
                AlertManager.alert(title: "TuDoc",mess: "Ingresa un correo valido" , viewcontroller: self);
                return
            }
        }
        
        if txt_password.text?.count == 0
        {
            AlertManager.alert(title: "TuDoc",mess: "Ingresa una contraseña" , viewcontroller: self);
            return;
        }
        self.keyboardHide()
        //SwiftSpinner.show("Entrando...")
        let user = txt_user.text!
        let password = txt_password.text!
        let dictParams: NSMutableDictionary? = ["username" : user,
                                                       "password" : password]
        SwiftSpinner.show("")
        invokeServ.requestForLoginDataWith(dictParams!) { [self] (result, error) in
            SwiftSpinner.hide()
                if result != nil
                {
                    
                    if result!["error"] != nil {
                        DispatchQueue.main.async
                        {
                            //SwiftSpinner.hide()
                            AlertManager.alert(title: "TuDoc",mess: "Revisa tu usuario o contraseña" , viewcontroller: self);
                            return;
                        }
                       
                    }
                    else
                    {
                    
                    
                     let dictParamsToken: NSMutableDictionary? = ["grant_type":"password" as Any, "username" : user, "password" : password]
                    let clientid = result!["client_id"] as! String
                    let clientsecret = result!["client_secret"] as! String
                    let user_id = result!["user_id"] as! Int
                    GlobalMembers.preferences.set(String(user_id), forKey: GlobalMembers.user_idKey)
                    let longstring = clientid + ":" + clientsecret
                    let data = (longstring).data(using: String.Encoding.utf8)
                    let basic_token = data!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
                    
                        self.invokeServ.requestForTokenDataWith(dictParamsToken as! [NSMutableDictionary : NSMutableDictionary], token: basic_token) { (result, error) in
                        
                            if result != nil
                            {
                                let accesstoken = result!["access_token"] as! String
                              
                                let token = "Bearer " + accesstoken
                              
                                GlobalMembers.preferences.set(token, forKey: GlobalMembers.tokenKey)
                                GlobalMembers.preferences.set(basic_token, forKey: GlobalMembers.basicTokenKey)
                                GlobalMembers.preferences.set(user, forKey: GlobalMembers.emailKey)
                                GlobalMembers.preferences.set(password, forKey: GlobalMembers.passwordKey)
                                
                                let issave = GlobalMembers.preferences.synchronize()
                                if issave
                                {
                                    self.getPatient()
                                }
                               
                                
                                    
                                
                            }
                            else
                            {
                                
                               
                                    DispatchQueue.main.async
                                    {
                                        //SwiftSpinner.hide()
                                        AlertManager.alert(title: "TuDoc",mess: "Revisa tu usuario o contraseña" , viewcontroller: self);
                                    }
                               
                            }
                    
                        }
                    }
                        
                    
                }
                else
                {
                    DispatchQueue.main.async
                    {
                        //SwiftSpinner.hide()
                        AlertManager.alert(title: "TuDoc",mess: "Revisa tu usuario o contraseña" , viewcontroller: self);
                    }
                    
                }
                
            
            
            
            
            
        
            }
        
        
        
    }
    
    func isValidEmail(emailStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }

    
    func getPatient()
    {
        //SwiftSpinner.show("")
        invokeServ.requestPatientDataWith { (response, error, code) in
            
            if code == 200
            {
                DispatchQueue.main.async {
                    SwiftSpinner.hide()
                    if !self.fromMenu
                    {
                        //self.performSegue(withIdentifier: "segue_institutions", sender: nil)
                        self.dismiss(animated: true) {
                            self.delegateLV?.seguetoInstitutions()
                        }

                    }
                    else
                    {
                        
                        self.dismiss(animated: true) {
                            self.delegate?.refresh()
                        }
                    }
                    
                       
                   
               
                }
            }
            else
            {
                DispatchQueue.main.async {
                    SwiftSpinner.hide()
                }
            }
        }
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
//        if segue.identifier == "segue_institutions"
//        {
//            if isfromVaccine
//            {
//                let vc : InstitutionsVC = segue.destination as! InstitutionsVC
//                vc.vaccine = vaccine
//                vc.date = date
//                vc.quantity = quantity
//                vc.fromVaccine = true
//                vc.selected_hour = selected_hour
//                //vc.loginVC = self
//            }
//            else
//            {
//                let vc : InstitutionsVC = segue.destination as! InstitutionsVC
//                vc.exam = exam
//                vc.date = date
//                vc.quantity = quantity
//                vc.selected_hour = selected_hour
//                //vc.loginVC = self
//
//            }
//        }
        if segue.identifier == "segue_register"
        {
            let vc : RegisterVC = segue.destination as! RegisterVC
            vc.loginVC = self
            vc.delegate = self

        }
    }
    
    
    @IBAction func action_back(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func action_register(_ sender: Any) {
        self.performSegue(withIdentifier: "segue_register", sender: nil)
    }
    
    @IBAction func action_password(_ sender: Any) {
        self.performSegue(withIdentifier: "segue_recover", sender: nil)
    }
    
    func segueInstitution() {
        //self.performSegue(withIdentifier: "segue_institutions", sender: nil)
        self.dismiss(animated: true) {
            self.delegateLV?.seguetoInstitutions()
        }
    }
}
