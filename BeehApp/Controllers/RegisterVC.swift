//
//  RegisterVC.swift
//  BeehApp
//
//  Created by Arturo Escamilla on 13/04/21.
//

import UIKit
import SwiftSpinner

protocol RegisterVCDelegate {
    func segueInstitution()
}

class RegisterVC: UIViewController,UITextFieldDelegate {
    
    var delegate: RegisterVCDelegate?
    
    @IBOutlet weak var layoutTopView: NSLayoutConstraint!
    @IBOutlet weak var txtFieldName: UITextField!
    @IBOutlet weak var txtFieldLastName: UITextField!
    @IBOutlet weak var txtFieldSecondLastName: UITextField!
    @IBOutlet weak var txtFieldPhone: UITextField!
    @IBOutlet weak var txtFieldEmail: UITextField!
    @IBOutlet weak var txtFieldPass: UITextField!
    @IBOutlet weak var btnShowPass: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var scrollContent: UIScrollView!
    @IBOutlet weak var viewContainerScroll: UIView!
    @IBOutlet weak var btnCheckBox: UIButton!
    
    var movedHeight = 0
    @IBOutlet weak var textFieldEditable:UITextField!
    //var data = NSDictionary()
    var cedulaSearch = Bool()
    
    let invokeService = InvokeService()
    
    var loginVC: LoginVC!
    //var genderByProffesionalCardConsultation = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))

       view.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardDidShowNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardDidHideNotification, object: nil)
        
        
        self.txtFieldName.delegate=self
        self.txtFieldEmail.delegate=self
        self.txtFieldPass.delegate=self
        self.txtFieldLastName.delegate=self
        self.txtFieldSecondLastName.delegate=self
        self.txtFieldPhone.delegate = self
        self.scrollContent.isScrollEnabled=true
        if (self.view.frame.size.height>=668) {
            self.scrollContent.isScrollEnabled=false
        }

        // Do any additional setup after loading the view.
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {

        let info = notification.userInfo! as NSDictionary
        

        let kKeyBoardFrame = info.object(forKey: UIResponder.keyboardFrameEndUserInfoKey) as! CGRect

        
        let KeyboardY=kKeyBoardFrame.origin.y
        
        let endPositionText=textFieldEditable.frame.origin.y+textFieldEditable.frame.size.height+20

        if (endPositionText<KeyboardY) {
            textFieldEditable=nil
            return;
        }
        
        
        let animatedDistance = 80+endPositionText-KeyboardY
        let movementDistance = animatedDistance;
        let movement = -movementDistance;
        movedHeight = Int(-movement);
        self.scrollContent.setContentOffset(CGPoint(x: 0, y: movedHeight), animated: true)
    }

    @objc func keyboardWillHide(notification: NSNotification) {

      
        self.scrollContent.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    
    func cancelNumberPad(){
       
        self.txtFieldName.text = "";
        self.txtFieldLastName.text="";
        self.txtFieldSecondLastName.text="";
        self.verifyFields()
    }
    
    func verifyFields(){

        if self.txtFieldName.text!.count<=0 {
            return;
        }
        if self.txtFieldLastName.text!.count<=0 {
            return;
        }
        if self.txtFieldSecondLastName.text!.count<=0 {
            return;
        }
        if self.txtFieldEmail.text!.count<=0 {
            return;
        }
        if self.txtFieldPhone.text!.count < 10 || self.txtFieldPhone.text!.count <= 0 || self.txtFieldPhone.text!.count > 10 {
        
            return;
        }

        
        if self.txtFieldPass.text!.count<=0 {
            return;
        }
        if (!GlobalMethods.validateEmailWithString(email: txtFieldEmail.text!)) {
            return;
        }
        if (!self.btnCheckBox.isSelected) {
            return;
        }
    }
    
    @IBAction func actionBack(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actionShowPass(_ sender: Any) {
        
        if self.txtFieldPass.isSecureTextEntry {
            self.txtFieldPass.isSecureTextEntry=false
            self.btnShowPass.setTitle("OCULTAR CONTRASEÑA", for: UIControl.State.normal)
        }else{
            self.txtFieldPass.isSecureTextEntry=true
            self.btnShowPass.setTitle("MOSTRAR CONTRASEÑA", for: UIControl.State.normal)
        }
        
    }
    
    @IBAction func actionCheckBox(_ sender: Any) {
        if self.btnCheckBox.isSelected {
            self.btnCheckBox.isSelected = false;
        }
        else{
            self.btnCheckBox.isSelected = true;
            
        }
        self.verifyFields()
    }
    @IBAction func actionShowTerms(_ sender: Any) {
    }
    
    @IBAction func actionNext(_ sender: Any) {
        
        if self.txtFieldName.text!.count<=0 {

            AlertManager.alert(title: "TuDoc", mess: "Debes ingresar un Nombre", viewcontroller: self)
            return;
        }
        if self.txtFieldLastName.text!.count<=0 {
            
            AlertManager.alert(title: "TuDoc", mess: "Debes ingresar un Apellido Paterno", viewcontroller: self)
            return;
        }

        
        if self.txtFieldPhone.text!.count < 10 || self.txtFieldPhone.text!.count <= 0 || self.txtFieldPhone.text!.count > 10 {
            
            AlertManager.alert(title: "TuDoc", mess: "Debes ingresar un teléfono valido", viewcontroller: self)
        
            return;
        }

        
        if self.txtFieldEmail.text!.count<=0 {
          
            AlertManager.alert(title: "TuDoc", mess: "Debes ingresar un Email", viewcontroller: self)
            return;
        }
        if !GlobalMethods.validateEmailWithString(email: self.txtFieldEmail.text!) {

            AlertManager.alert(title: "TuDoc", mess: "Debes ingresar un Email válido", viewcontroller: self)
            return;
        }
        if self.txtFieldPass.text!.count<=0{
            AlertManager.alert(title: "TuDoc", mess: "Debes ingresar una contraseña", viewcontroller: self)
            return;
        }

        if !self.btnCheckBox.isSelected {
            AlertManager.alert(title: "TuDoc", mess: "Debes aceptar los Términos y Condiciones", viewcontroller: self)
            return;
        }

        let dict = NSMutableDictionary()
        dict.setValue(self.txtFieldEmail.text, forKey: "email")
        dict.setValue(self.txtFieldEmail.text, forKey: "username")
        dict.setValue(self.txtFieldPass.text, forKey: "password")
        dict.setValue(self.txtFieldName.text, forKey: "first_name")
        dict.setValue(self.txtFieldLastName.text, forKey: "last_name")
        dict.setValue(self.txtFieldSecondLastName.text, forKey: "second_last_name")
        dict.setValue(self.txtFieldPhone.text, forKey: "mobile_phone_number")
        
        let user = self.txtFieldEmail.text!
        let password = self.txtFieldPass.text!

        SwiftSpinner.show("")
        invokeService.registerWithData(dict) { (response, error, code) in
            if code == 201
            {
                let dictParams: NSMutableDictionary? = ["username" : user,
                                                               "password" : password]
                //SwiftSpinner.show("")
                self.invokeService.requestForLoginDataWith(dictParams!) { [self] (result, error) in
                    SwiftSpinner.hide()
                        if result != nil
                        {
                            
                            if result!["error"] != nil {
                                DispatchQueue.main.async
                                {
                                    SwiftSpinner.hide()
                                    AlertManager.alert(title: "TuDoc",mess: "Revisa tu usuario o contraseña" , viewcontroller: self);
                                    return;
                                }
                               
                            }
                            else
                            {
                            
                            
                             let dictParamsToken: NSMutableDictionary? = ["grant_type":"password" as Any, "username" : user, "password" : password]
                            let clientid = result!["client_id"] as! String
                            let clientsecret = result!["client_secret"] as! String
                            let user_id = result!["user_id"] as! Int
                            GlobalMembers.preferences.set(String(user_id), forKey: GlobalMembers.user_idKey)
                            let longstring = clientid + ":" + clientsecret
                            let data = (longstring).data(using: String.Encoding.utf8)
                            let basic_token = data!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
                            
                                self.invokeService.requestForTokenDataWith(dictParamsToken as! [NSMutableDictionary : NSMutableDictionary], token: basic_token) { (result, error) in
                                
                                    if result != nil
                                    {
                                        let accesstoken = result!["access_token"] as! String
                                      
                                        let token = "Bearer " + accesstoken
                                      
                                        GlobalMembers.preferences.set(token, forKey: GlobalMembers.tokenKey)
                                        GlobalMembers.preferences.set(basic_token, forKey: GlobalMembers.basicTokenKey)
                                        GlobalMembers.preferences.set(user, forKey: GlobalMembers.emailKey)
                                        GlobalMembers.preferences.set(password, forKey: GlobalMembers.passwordKey)
                                        
                                        let issave = GlobalMembers.preferences.synchronize()
                                        if issave
                                        {
                                            DispatchQueue.main.async
                                            {
                                                SwiftSpinner.hide()
 
                                                    self.dismiss(animated: true) {
                                                        self.delegate?.segueInstitution()
                                                    }
      
                                                
                                            }
                                        }
                                       
                                        
                                                
                                        
                                    }
                                    else
                                    {
                                        
                                       
                                            DispatchQueue.main.async
                                            {
                                                SwiftSpinner.hide()
                                                AlertManager.alert(title: "TuDoc",mess: "Revisa tu usuario o contraseña" , viewcontroller: self);
                                            }
                                       
                                    }
                            
                                }
                            }
                                
                            
                        }
                        else
                        {
                            DispatchQueue.main.async
                            {
                                SwiftSpinner.hide()
                                AlertManager.alert(title: "TuDoc",mess: "Revisa tu usuario o contraseña" , viewcontroller: self);
                            }
                            
                        }
                        
                    
                    
                    
                    
                    
                
                    }
                
                
            }
            else
            {
                DispatchQueue.main.async {
                    SwiftSpinner.hide()
                    if response!.count > 0
                    {
                        AlertManager.alert(title: "TuDoc", mess: "Este usuario ya existe!", viewcontroller: self)
                    }
                    else
                    {
                    
                        AlertManager.alert(title: "TuDoc", mess: "Error al Iniciar Sesión", viewcontroller: self)
                    }
                }
            }
        }
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        self.scrollContent.isScrollEnabled = true
        textFieldEditable=textField;

    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.txtFieldPhone
        {
            if string.count == 0
            {
                return true;
            }
            
            if self.txtFieldPhone.text!.count >= 10
            {
                return false;
            }
        }
        return true;
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

class TextField: UITextField {

    let padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)

    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}
