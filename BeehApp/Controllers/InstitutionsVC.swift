//
//  InstitutionsVC.swift
//  BeehApp
//
//  Created by Arturo Escamilla on 12/04/21.
//

import UIKit
import SwiftSpinner

class InstitutionsCell:UITableViewCell
{
    
    @IBOutlet weak var lbl_address: UILabel!
    @IBOutlet weak var lbl_institution: UILabel!
}

class InstitutionsVC: UIViewController,UITableViewDelegate, UITableViewDataSource {
   
    @IBOutlet weak var tv_intitutions: UITableView!
    var array_institutions = NSMutableArray()
    
    var exam = Exam()
    var vaccine = Vaccine()
    var date = String()
    var quantity = Int()
    var insite = Bool()
    var fromVaccine = Bool()
    var selected_hour = NSArray()
    
    var selected_institution = Institution()

    var refreshControl = UIRefreshControl()
    //var loginVC: LoginVC!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        if loginVC != nil
//        {
//            loginVC.dismiss(animated: true, completion: nil)
//        }

        self.refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tv_intitutions.addSubview(refreshControl)
        tv_intitutions.backgroundColor = UIColor.white
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        getInstitutions()
    }

    func getInstitutions(){
        SwiftSpinner.show("")
        invokeService.requestInstitutionsDataWith { (response, error, code) in
            if code == 200
            {
                self.array_institutions = NSMutableArray()
                self.array_institutions = self.filterData(institutes: response!)
                DispatchQueue.main.async {
                    SwiftSpinner.hide()
                    self.refreshControl.endRefreshing()
                    self.tv_intitutions.reloadData()
                }
            }
        }
    }
    
    func filterData(institutes:NSArray) -> NSMutableArray {
        
        let array_result = NSMutableArray()
        
        for inst in institutes
        {
            //let vac_entitie = VaccineCode()
            let institution_entitie = Institution()
            
            let institution = inst as! NSDictionary
            
            institution_entitie.id = institution.object(forKey: "id") as? Int
            institution_entitie.name = institution.object(forKey: "name") as? String
            institution_entitie.address = institution.object(forKey: "address") as? String
            
            institution_entitie.is_active = institution.object(forKey: "is_active") as? Bool

            
            array_result.add(institution_entitie)
            
            
            
        }
        
        return array_result
        
    }
    @objc func refresh(_ sender: AnyObject) {
        self.getInstitutions()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array_institutions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_institution", for: indexPath) as! InstitutionsCell
        
        let institution = array_institutions.object(at: indexPath.row) as! Institution
        cell.lbl_institution.text = institution.name
        cell.lbl_address.text = institution.address
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let institution = array_institutions.object(at: indexPath.row) as! Institution
        self.selected_institution = institution
        if fromVaccine
        {
            self.performSegue(withIdentifier: "segue_payment_vaccine", sender: nil)
        }
        else
        {
            self.performSegue(withIdentifier: "segue_payment_exam", sender: nil)
        }
        
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "segue_payment_exam"
        {
            let vc : PaymentExamVC = segue.destination as! PaymentExamVC
            vc.exam = exam
            vc.date = date
            vc.quantity = quantity
            vc.selected_hour = selected_hour
            vc.institution = selected_institution
            
        }
        
        else
        {
            let vc : PaymentVaccineVC = segue.destination as! PaymentVaccineVC
            vc.vaccine = vaccine
            vc.date = date
            vc.quantity = quantity
            vc.selected_hour = selected_hour
            vc.institution = selected_institution
            //vc.loginVC = loginVC
            
        }
        
    }
    
    
    @IBAction func action_back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

}
