//
//  OptionsVC.swift
//  BeehApp
//
//  Created by Arturo Escamilla on 10/03/21.
//

import UIKit
import SwiftSpinner
class OptionsCell: UITableViewCell {
    
    @IBOutlet weak var lbl_name: UILabel!
}

class OptionsVC: UIViewController, UITableViewDataSource, UITableViewDelegate, OptionsVCDelegate {
    
    @IBOutlet weak var lbl_welcome: UILabel!
    
    @IBOutlet weak var lbl_name: UILabel!
    
    @IBOutlet weak var tv_options: UITableView!
    
    var array_options = NSMutableArray()
    
    let invokeServ = InvokeService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lbl_name.isHidden = true
        lbl_welcome.isHidden = true
        tv_options.backgroundColor = UIColor.white
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        createMenu()
    }
    
    func createMenu() {
        
        array_options = NSMutableArray()
        
        let dic_option1 = NSMutableDictionary()
        dic_option1.setValue("Ordenes de Examenes", forKey: "name")
        
        let dic_option2 = NSMutableDictionary()
        dic_option2.setValue("Ordenes de Vacunas", forKey: "name")
        
        let dic_option3 = NSMutableDictionary()
        dic_option3.setValue("Cerrar sesión", forKey: "name")
        
        let dic_option4 = NSMutableDictionary()
        dic_option4.setValue("Iniciar sesión", forKey: "name")
        
        let dic_option5 = NSMutableDictionary()
        dic_option5.setValue("Registrar", forKey: "name")
        
        
        
        let token = GlobalMembers.preferences.object(forKey: GlobalMembers.tokenKey) as? String
        if token != nil
        {
            array_options.add(dic_option1)
            array_options.add(dic_option2)
            array_options.add(dic_option3)
            self.getPatient()
        }
        else
        {
            array_options.add(dic_option4)
            array_options.add(dic_option5)
            lbl_name.isHidden = true
            lbl_welcome.isHidden = true
        }

        tv_options.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array_options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell_options", for: indexPath) as! OptionsCell
        
        let dic = array_options.object(at: indexPath.row) as! NSDictionary
        cell.lbl_name.text = (dic.object(forKey: "name") as! String)
    
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let token = GlobalMembers.preferences.object(forKey: GlobalMembers.tokenKey) as? String
        if token != nil
        {
            switch indexPath.row {
            case 0:
                performSegue(withIdentifier: "segue_order_exam", sender: nil)
                break
            case 1:
                performSegue(withIdentifier: "segue_order_vac", sender: nil)
                break
            case 2:
                endsession()
                break
            default:
                break
            }
        }
        else
        {
            switch indexPath.row {
            case 0:
                performSegue(withIdentifier: "login_segue", sender: nil)
                break
            case 1:
                performSegue(withIdentifier: "segue_register", sender: nil)
                break
            default:
                break
            }
            
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func getPatient()
    {
        SwiftSpinner.show("")
        invokeServ.requestPatientDataWith { (response, error, code) in
            SwiftSpinner.hide()
            if code == 200
            {
                DispatchQueue.main.async { [self] in
                    
                    let dic_pat = response![0] as! NSDictionary
                    
                    let gender = dic_pat.object(forKey: "gender") as! String
                    
                    var welcome = "Bienvenido "
                    
                    if gender == "female"
                    {
                        welcome = "Bienvenida "
                    }
                    
                    
                    let first_name = dic_pat.object(forKey: "first_name") as! String
                    let last_name = dic_pat.object(forKey: "last_name") as! String
                    let second_last_name = dic_pat.object(forKey: "second_last_name") as! String
                    
                    let complete_name = first_name + " " + last_name + " " + second_last_name
                    
                    self.lbl_name.isHidden = false
                    self.lbl_welcome.isHidden = false
                    self.lbl_name.text = complete_name.uppercased()
                    self.lbl_welcome.text = welcome.uppercased()
                    
               
                }
            }
        }
    }

    func endsession()  {
        
        GlobalMembers.preferences.removeObject(forKey: GlobalMembers.emailKey)
        GlobalMembers.preferences.removeObject(forKey: GlobalMembers.passwordKey)
        GlobalMembers.preferences.removeObject(forKey: GlobalMembers.tokenKey)
        GlobalMembers.preferences.removeObject(forKey: GlobalMembers.basicTokenKey)
        createMenu()
        
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "login_segue"
        {
            let vc : LoginVC = segue.destination as! LoginVC
            vc.fromMenu = true
            vc.delegate = self
        }
    }
    
    func refresh() {
        createMenu()
    }
    

}
