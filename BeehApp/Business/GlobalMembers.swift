//
//  GlobalMembers.swift
//  BeehApp
//
//  Created by Arturo Escamilla on 10/03/21.
//

import UIKit

class GlobalMembers: NSObject {
    
    static var preferences = UserDefaults.standard
    static var address_key = "address_key"
    static var tokenKey = "currentToken"
    static var user_idKey = "user_idKey"
    static var basicTokenKey = "basicToken"
    static var emailKey = "emailKey"
    static var passwordKey = "passwordKey"
    

    
}
