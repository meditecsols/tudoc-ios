//
//  Config.swift
//  BeehApp
//
//  Created by Arturo Escamilla on 10/03/21.
//

import UIKit

class Config{
    static let URL_PROD = "https://api.family-doc.com/"
    static let URL_SANDBOX = "https://sandbox.family-doc.com/"
    static let BASE_URL = URL_SANDBOX
    
    static let CONEKTA_PROD = "key_WpswV8fDLffDQhRvcHvfzwQ"
    static let CONEKTA_SANDBOX = "key_Oo3xASbL4ae2cSzmw1vKsEQ"
    static let CONEKTA_KEY = CONEKTA_SANDBOX

}
