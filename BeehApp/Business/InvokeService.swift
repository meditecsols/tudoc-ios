//
//  InvokeService.swift
//  BeehApp
//
//  Created by Arturo Escamilla on 11/03/21.
//

import UIKit

class InvokeService: NSObject {
    
    let user_id = GlobalMembers.preferences.string(forKey: GlobalMembers.user_idKey)
    
    func requestForLoginDataWith(_ params: NSMutableDictionary, completionHandler: @escaping (_ result: NSDictionary?, _ error: Error?) -> Void){
        //let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
        let url = Config.BASE_URL+"api/v0/login/"
        var request = URLRequest(url: URL(string: url)!)
               request.httpMethod = "POST"
               request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse
                 print(httpResponse!.statusCode)
                if httpResponse!.statusCode == 200 {
                     // let errors: NSError?
                    //errors = error! as NSError
                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        if dictonary!.count == 1
                        {
                            
                            completionHandler(dictonary, nil)
                        }
                        else
                        {
                            
                             completionHandler(dictonary, error)
                        }
                          
                      } catch let error as NSError {
                         completionHandler(nil, error)
                      }
                }
                else
                {
                     completionHandler(nil, error)
                }
                 
                  
                    //completionHandler(json, error!)
        
        
               })

               task.resume()
    }
    func requestForTokenDataWith(_ params: [NSMutableDictionary: NSMutableDictionary],token:String, completionHandler: @escaping (_ result: NSDictionary?, _ error: Error?) -> Void){
        //let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
        var request = URLRequest(url: URL(string: Config.BASE_URL+"o/token/")!)
               request.httpMethod = "POST"
               request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
               request.addValue("Basic " + token, forHTTPHeaderField: "Authorization")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse
                if httpResponse!.statusCode == 200 {
                     // let errors: NSError?
                    //errors = error! as NSError
                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        if dictonary!.count == 1
                        {
                           
                            completionHandler(dictonary,  error)
                        }
                        else
                        {
                             completionHandler(dictonary, error)
                        }
                          
                      } catch let error as NSError {
                         completionHandler(nil, error)
                      }
                }
                else
                {
                    completionHandler(nil, error)
                }
                 
                  
                    //completionHandler(json, error!)
        
        
               })

               task.resume()
    }
    
    func refreshTokenDataWith(completionHandler: @escaping (_ result: Bool?, _ error: Error?) -> Void){
        let token = GlobalMembers.preferences.string(forKey: GlobalMembers.basicTokenKey)
        let password = GlobalMembers.preferences.string(forKey: GlobalMembers.passwordKey)
        let email = GlobalMembers.preferences.string(forKey: GlobalMembers.emailKey)
        
        let params: NSMutableDictionary? = ["grant_type":"password" as Any, "username" : email!, "password" : password!]
        //let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
        var request = URLRequest(url: URL(string: Config.BASE_URL+"o/token/")!)
               request.httpMethod = "POST"
               request.httpBody = try? JSONSerialization.data(withJSONObject: params!, options: [])
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
               request.addValue("Basic " + token!, forHTTPHeaderField: "Authorization")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse
                if httpResponse!.statusCode == 200 {
                     // let errors: NSError?
                    //errors = error! as NSError
                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                       
                        let accesstoken = dictonary!["access_token"] as! String
                        
                        let token = "Bearer " + accesstoken
                        
                        GlobalMembers.preferences.set(token, forKey: GlobalMembers.tokenKey)

                        
                        let didSave = GlobalMembers.preferences.synchronize()

                        if didSave {
                            
                            completionHandler(true, error)
                        }
                          
                      } catch let error as NSError {
                         completionHandler(false, error)
                      }
                }
                else
                {
                    completionHandler(false, error)
                }
                 
                  
                    //completionHandler(json, error!)
        
        
               })

               task.resume()
    }
    
    // Obtener Lista de Vacunas
    
    func requestVaccinesDataWith(zipcode:String, completionHandler: @escaping (_ result: NSArray?, _ error: Error?,_ code: Int) -> Void){
        
        var token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
        if token == nil
        {
            token = ""
        }
        //var request = URLRequest(url: URL(string: Config.BASE_URL+"api/v0/vaccine/" + zipcode + "/by_zipcode/")!)
        var request = URLRequest(url: URL(string: Config.BASE_URL+"api/v0/vaccine/by_active/")!)
               request.httpMethod = "GET"
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
               request.addValue( token!, forHTTPHeaderField: "Authorization")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse
                

                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSArray

                         completionHandler(dictonary ,  error, httpResponse!.statusCode)
                      

                      } catch let error as NSError {
                         completionHandler(nil, error, httpResponse!.statusCode)
                      }
                


                    //completionHandler(json, error!)


               })

               task.resume()
    }
    
    // Obtener Window work de Vaccines
    
    func requestVaccineWindowWorkingDataWith(id:String, completionHandler: @escaping (_ result: NSArray?, _ error: Error?, _ code: Int) -> Void){
        

        var request = URLRequest(url: URL(string: Config.BASE_URL+"api/v0/vaccineworkingwindow/" + id + "/by_vaccine/")!)
               request.httpMethod = "GET"
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
               //request.addValue( token!, forHTTPHeaderField: "Authorization")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse

                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSArray

                         completionHandler(dictonary ,  error, httpResponse!.statusCode)
                      

                      } catch let error as NSError {
                         completionHandler(nil, error, httpResponse!.statusCode)
                      }



               })

               task.resume()
    }
    

    // Obtener Disponibilidad Vacunas
    
    func requestAvailabilityVaccineDataWith(id:String,date:String,slotsz:String, completionHandler: @escaping (_ result: NSArray?, _ error: Error?, _ code: Int) -> Void){
        

        var request = URLRequest(url: URL(string: Config.BASE_URL+"api/v0/vaccine/" + id + "/availability/?date=" + date + "&slotsz=" + slotsz)!)
               request.httpMethod = "GET"
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
               //request.addValue( token!, forHTTPHeaderField: "Authorization")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse

                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSArray

                         completionHandler(dictonary ,  error, httpResponse!.statusCode)
                      

                      } catch let error as NSError {
                         completionHandler(nil, error, httpResponse!.statusCode)
                      }



               })

               task.resume()
    }
    
    //Obtener info de paciente
    
    func requestPatientDataWith(completionHandler: @escaping (_ result: NSArray?, _ error: Error?, _ code: Int) -> Void){
        let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
        var request = URLRequest(url: URL(string: Config.BASE_URL+"api/v0/patient/")!)
               request.httpMethod = "GET"
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue(token!, forHTTPHeaderField: "Authorization")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse

                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSArray

                         completionHandler(dictonary ,  error, httpResponse!.statusCode)
                      

                      } catch let error as NSError {
                         completionHandler(nil, error, httpResponse!.statusCode)
                      }



               })

               task.resume()
        
        
        
    }
    
    
    func requestForChargeDataWith(_ params: NSMutableDictionary, completionHandler: @escaping (_ result: NSDictionary?, _ error: Error?, _ code : Int?) -> Void){
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: params,
            options: []) {
            let theJSONText = String(data: theJSONData,
                                       encoding: .ascii)
            print("JSON string = \(theJSONText!)")
        }
        
        let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
        let url = Config.BASE_URL+"api/v0/conektalink/chargevaccineconekta/"
        
        var request = URLRequest(url: URL(string: url)!)
               request.httpMethod = "POST"


               request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
               request.addValue(token!, forHTTPHeaderField: "Authorization")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse
                 print(httpResponse!.statusCode)
 
                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary

                             completionHandler(dictonary, error,httpResponse!.statusCode)
                        
                          
                      } catch let error as NSError {
                        let returnData = String(data: data!, encoding: .utf8)
                        let dic = NSMutableDictionary()
                        dic.setValue(returnData, forKey: "message")
                         completionHandler(dic, error,httpResponse!.statusCode)
                      }

                 
                  
                    //completionHandler(json, error!)
        
        
               })

               task.resume()
    }
    
    
    // Obtener Lista de Examenes
    
    func requestExamsDataWith(zipcode:String, completionHandler: @escaping (_ result: NSArray?, _ error: Error?,_ code: Int) -> Void){
        
        var token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
        if token == nil
        {
            token = ""
        }
        //var request = URLRequest(url: URL(string: Config.BASE_URL+"api/v0/exam/" + zipcode + "/by_zipcode/")!)
        var request = URLRequest(url: URL(string: Config.BASE_URL+"api/v0/exam/by_active/")!)
               request.httpMethod = "GET"
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
               request.addValue( token!, forHTTPHeaderField: "Authorization")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse
                

                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSArray

                         completionHandler(dictonary ,  error, httpResponse!.statusCode)
                      

                      } catch let error as NSError {
                         completionHandler(nil, error, httpResponse!.statusCode)
                      }
                


                    //completionHandler(json, error!)


               })

               task.resume()
    }
    
    
    // Obtener Window work de Exams
    
    func requestExamWindowWorkingDataWith(id:String, completionHandler: @escaping (_ result: NSArray?, _ error: Error?, _ code: Int) -> Void){
        

        var request = URLRequest(url: URL(string: Config.BASE_URL+"api/v0/examworkingwindow/" + id + "/by_exam/")!)
               request.httpMethod = "GET"
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
               //request.addValue( token!, forHTTPHeaderField: "Authorization")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse

                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSArray

                         completionHandler(dictonary ,  error, httpResponse!.statusCode)
                      

                      } catch let error as NSError {
                         completionHandler(nil, error, httpResponse!.statusCode)
                      }



               })

               task.resume()
    }
    
    // Obtener Disponibilidad Exam
    
    func requestAvailabilityExamDataWith(id:String,date:String,slotsz:String, completionHandler: @escaping (_ result: NSArray?, _ error: Error?, _ code: Int) -> Void){
        

        var request = URLRequest(url: URL(string: Config.BASE_URL+"api/v0/exam/" + id + "/availability/?date=" + date + "&slotsz=" + slotsz)!)
               request.httpMethod = "GET"
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
               //request.addValue( token!, forHTTPHeaderField: "Authorization")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse

                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSArray

                         completionHandler(dictonary ,  error, httpResponse!.statusCode)
                      

                      } catch let error as NSError {
                         completionHandler(nil, error, httpResponse!.statusCode)
                      }



               })

               task.resume()
    }
    
    //Cargo para examen
    
    func requestForChargeExamDataWith(_ params: NSMutableDictionary, completionHandler: @escaping (_ result: NSDictionary?, _ error: Error?, _ code : Int?) -> Void){
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: params,
            options: []) {
            let theJSONText = String(data: theJSONData,
                                       encoding: .ascii)
            print("JSON string = \(theJSONText!)")
        }
        
        let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
        let url = Config.BASE_URL+"api/v0/conektalink/chargeexamconekta/"
        
        var request = URLRequest(url: URL(string: url)!)
               request.httpMethod = "POST"


               request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
               request.addValue(token!, forHTTPHeaderField: "Authorization")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse
                 print(httpResponse!.statusCode)
 
                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary

                             completionHandler(dictonary, error,httpResponse!.statusCode)
                        
                          
                      } catch let error as NSError {
                        let returnData = String(data: data!, encoding: .utf8)
                        let dic = NSMutableDictionary()
                        dic.setValue(returnData, forKey: "message")
                         completionHandler(dic, error,httpResponse!.statusCode)
                      }

                 
                  
                    //completionHandler(json, error!)
        
        
               })

               task.resume()
    }
    
    // Obtener Ordenes de Examenes
    
    func requestOrderExamDataWith( completionHandler: @escaping (_ result: NSArray?, _ error: Error?, _ code: Int) -> Void){
        
        let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)

        var request = URLRequest(url: URL(string: Config.BASE_URL+"api/v0/examorder/by_patient/")!)
               request.httpMethod = "GET"
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
               request.addValue( token!, forHTTPHeaderField: "Authorization")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse

                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSArray

                         completionHandler(dictonary ,  error, httpResponse!.statusCode)
                      

                      } catch let error as NSError {
                         completionHandler(nil, error, httpResponse!.statusCode)
                      }



               })

               task.resume()
    }
    
    // Obtener Examen por id
    
    func requestExambyIdDataWith(id:String, completionHandler: @escaping (_ result: NSDictionary?, _ error: Error?, _ code: Int) -> Void){
        
        let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)

        var request = URLRequest(url: URL(string: Config.BASE_URL+"api/v0/exam/" + id + "/")!)
               request.httpMethod = "GET"
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
               request.addValue( token!, forHTTPHeaderField: "Authorization")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse

                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary

                         completionHandler(dictonary ,  error, httpResponse!.statusCode)
                      

                      } catch let error as NSError {
                         completionHandler(nil, error, httpResponse!.statusCode)
                      }



               })

               task.resume()
    }
    
    
    // Obtener Ordenes de Vacunas
    
    func requestOrderVaccineDataWith( completionHandler: @escaping (_ result: NSArray?, _ error: Error?, _ code: Int) -> Void){
        
        let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)

        var request = URLRequest(url: URL(string: Config.BASE_URL+"api/v0/vaccineorder/by_patient/")!)
               request.httpMethod = "GET"
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
               request.addValue( token!, forHTTPHeaderField: "Authorization")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse

                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSArray

                         completionHandler(dictonary ,  error, httpResponse!.statusCode)
                      

                      } catch let error as NSError {
                         completionHandler(nil, error, httpResponse!.statusCode)
                      }



               })

               task.resume()
    }
    
    // Obtener Vacuna por id
    
    func requestVaccinebyIdDataWith(id:String, completionHandler: @escaping (_ result: NSDictionary?, _ error: Error?, _ code: Int) -> Void){
        
        let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)

        var request = URLRequest(url: URL(string: Config.BASE_URL+"api/v0/vaccine/" + id)!)
               request.httpMethod = "GET"
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
               request.addValue( token!, forHTTPHeaderField: "Authorization")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse

                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary

                         completionHandler(dictonary ,  error, httpResponse!.statusCode)
                      

                      } catch let error as NSError {
                         completionHandler(nil, error, httpResponse!.statusCode)
                      }



               })

               task.resume()
    }
    
    // Obtener Institucion por id
    
    func requestInstitutionbyIdDataWith(id:String, completionHandler: @escaping (_ result: NSDictionary?, _ error: Error?, _ code: Int) -> Void){
        
        let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)

        var request = URLRequest(url: URL(string: Config.BASE_URL+"api/v0/institution/" + id + "/")!)
               request.httpMethod = "GET"
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
               request.addValue( token!, forHTTPHeaderField: "Authorization")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse

                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary

                         completionHandler(dictonary ,  error, httpResponse!.statusCode)
                      

                      } catch let error as NSError {
                         completionHandler(nil, error, httpResponse!.statusCode)
                      }



               })

               task.resume()
    }
    
    // Obtener Instituciones
    
    func requestInstitutionsDataWith(completionHandler: @escaping (_ result: NSArray?, _ error: Error?, _ code: Int) -> Void){
        
        let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)

        var request = URLRequest(url: URL(string: Config.BASE_URL+"api/v0/institution/")!)
               request.httpMethod = "GET"
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")
               request.addValue( token!, forHTTPHeaderField: "Authorization")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse

                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSArray

                         completionHandler(dictonary ,  error, httpResponse!.statusCode)
                      

                      } catch let error as NSError {
                         completionHandler(nil, error, httpResponse!.statusCode)
                      }



               })

               task.resume()
    }
    
    func registerWithData(_ params: NSMutableDictionary, completionHandler: @escaping (_ result: NSDictionary?, _ error: Error?, _ code: Int) -> Void){
        //let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
        let url = Config.BASE_URL+"api/v0/patient/"
        var request = URLRequest(url: URL(string: url)!)
               request.httpMethod = "POST"
               request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse
                 print(httpResponse!.statusCode)
                if httpResponse!.statusCode == 201 {
                     // let errors: NSError?
                    //errors = error! as NSError
                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary

                            
                             completionHandler(dictonary, error,httpResponse!.statusCode)
                        
                          
                      } catch let error as NSError {
                         completionHandler(nil, error,httpResponse!.statusCode)
                      }
                }
                else
                {
                    do {
                    let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                     completionHandler(dictonary, error,httpResponse!.statusCode)
                    } catch let error as NSError {
                       completionHandler(nil, error,httpResponse!.statusCode)
                    }
                        
                }
                 
                  
                    //completionHandler(json, error!)
        
        
               })

               task.resume()
    }
    
    func recoverWithData(_ params: NSMutableDictionary, completionHandler: @escaping (_ result: NSDictionary?, _ error: Error?, _ code: Int) -> Void){
        //let token = GlobalMembers.preferences.string(forKey: GlobalMembers.tokenKey)
        let url = Config.BASE_URL+"api/v0/login/recover/viaemail/"
        var request = URLRequest(url: URL(string: url)!)
               request.httpMethod = "POST"
               request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
               request.addValue("application/json", forHTTPHeaderField: "Content-Type")

               let session = URLSession.shared
               let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                   print(response!)
                let httpResponse = response as? HTTPURLResponse
                 print(httpResponse!.statusCode)
                if httpResponse!.statusCode == 201 {
                     // let errors: NSError?
                    //errors = error! as NSError
                      //let returnData = String(data: data!, encoding: .utf8)
                      do {
                        let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary

                            
                             completionHandler(dictonary, error,httpResponse!.statusCode)
                        
                          
                      } catch let error as NSError {
                         completionHandler(nil, error,httpResponse!.statusCode)
                      }
                }
                else
                {
                    do {
                    let dictonary =  try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                     completionHandler(dictonary, error,httpResponse!.statusCode)
                    } catch let error as NSError {
                       completionHandler(nil, error,httpResponse!.statusCode)
                    }
                        
                }
                 
                  
                    //completionHandler(json, error!)
        
        
               })

               task.resume()
    }
}
