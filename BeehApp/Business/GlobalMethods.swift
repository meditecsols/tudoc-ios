//
//  GlobalMethods.swift
//  BeehApp
//
//  Created by Arturo Escamilla on 11/03/21.
//

import UIKit

class GlobalMethods: NSObject {
    
    static func getZipCode(place:NSDictionary) -> String{
        
        let address_components = place.value(forKey: "address_components") as! NSArray
        
        
        for component in address_components
        {
            let comp = component as! NSDictionary
            let types = comp.value(forKey: "types") as! NSArray
            
           if( self.detectPostal(type: types))
           {
                return comp.value(forKey: "short_name") as! String
                
           }
            
        }
        
        return ""
        
        
    }
    
    static func getFormatedAddress(place:NSDictionary) -> String{
        
        let format_address = place.object(forKey: "formatted_address") as? String
        
        return format_address!

    }
    
    static func detectPostal(type:NSArray) -> Bool {
        
        for item in type {
            if(item as! String == "postal_code")
            {
                return true
            }
            
        }
        
        return false
    }
    
    
    static func convertDoubleToCurrency(amount: Double) -> String{
           let numberFormatter = NumberFormatter()
           numberFormatter.numberStyle = .currency
           numberFormatter.locale = Locale.current
           return numberFormatter.string(from: NSNumber(value: amount))!
    }
    static func convertCurrencyToDouble(input: String) -> Double? {
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = .currency
            numberFormatter.locale = Locale.current
            return numberFormatter.number(from: input)?.doubleValue
    }
    
    static func setHourService(date:String) -> String
    {
        let dateFormatter = DateFormatter()
        //let tempLocale = dateFormatter.locale // save locale temporarily
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let date = dateFormatter.date(from: date)!
        dateFormatter.dateFormat = "h:mm a"
        //dateFormatter.locale = tempLocale // reset the locale
        let dateString = dateFormatter.string(from: date)

        return dateString
        
    }
    
    
    static func setDate(date:String) -> String
    {
        let dateFormatter = DateFormatter()
        //let tempLocale = dateFormatter.locale // save locale temporarily
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let date = dateFormatter.date(from: date)!
        dateFormatter.dateFormat = "dd-MMM-yyyy HH:mm a"
        //dateFormatter.locale = tempLocale // reset the locale
        let dateString = dateFormatter.string(from: date)

        return dateString
        
    }
    
    static func validateEmailWithString(email : String) -> Bool
    {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"

        
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        
        return emailTest.evaluate(with: email)
    }
    
    func validatePhone(phoneNumber : String) -> Bool
    {
        let phoneRegex = "^((\\+)|(00))[0-9]{6,14}$"
    
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)

        return phoneTest.evaluate(with: phoneNumber)
    }
    
    static func validatePassWithString(pass : String) -> Bool{
        if (pass.count > 0) {
            return true;
        }
        return false;
    }
    
}
